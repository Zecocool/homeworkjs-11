"use strict"
const showPassword = document.querySelectorAll(".icon-password");
const inputPassword = document.getElementsByTagName("input");
const buttonSubmit = document.querySelector(".btn");


const showPasswordArray = [...showPassword];
const inputPasswordArray = [...inputPassword];

function eyeSlash(element) {
  element.classList.remove("fa-eye");
  element.classList.add("fa-eye-slash");
}

function eyeSlashRemove(element) {
  element.classList.remove("fa-eye-slash");
  element.classList.add("fa-eye");
}

if (Array.isArray(showPasswordArray)) {
  showPasswordArray.forEach((element, index) => {
    element.addEventListener("click" , () => {
      if (element.classList.contains("fa-eye")) {
        eyeSlash(element);
        inputPasswordArray[index].setAttribute("type", "text");
      } else {
        eyeSlashRemove(element);
        inputPasswordArray[index].setAttribute("type", "password");
      }
    });
  });
}

const warning = document.createElement("p");

buttonSubmit.addEventListener("click", (event)=>{
  event.preventDefault();
  if (inputPasswordArray[0].value !== inputPasswordArray[1].value) {
    inputPasswordArray[1].parentElement.append(warning);
    warning.style.color = "red";
    warning.innerText = "";
    warning.innerText = "Ведіть будь-ласка однакові значення";
  } else if (
    inputPasswordArray[0].value.length > 0 &&
    inputPasswordArray[1].value.length > 0 &&
    inputPasswordArray[0].value === inputPasswordArray[1].value
  ) {
    warning.remove();
    alert("You are welcome");
  } else {
    warning.remove();
  }
});

